import 'package:flutter/material.dart';
Widget RectangleImage()
{
  return Container(
    width: 150.0,
    height: 150.0,
    decoration: new BoxDecoration(
        shape: BoxShape.rectangle,
        border: Border.all(color: Colors.indigoAccent, width: 10.0, style: BorderStyle.solid),
        image: new DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("images/figure.jpeg")
        )
    ),
  );
}