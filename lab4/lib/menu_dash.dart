import 'package:flutter/material.dart';
//import rectangular_image.dart
import 'rect_image.dart';

//Create a stateful widget for accessible menu
class Menu extends StatefulWidget
{
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu>
{
  bool isMenuClosed = true;
  double screenWidth;

  @override
  Widget build(BuildContext context)
  {
    Size size = MediaQuery
        .of(context)
        .size;
    screenWidth = size.width;

    return Scaffold
      (
      backgroundColor: Colors.red[850],
      body: Stack(
        children: <Widget>[
          menu(context),
          dashboard(context),

        ],
      ),
    );
  }

  //Create  menu widget
  Widget menu(context)
  {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, screenWidth * 0.15, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          //By using Flexible it gives the widget the ability to expand and fill,
          //horizontally and vertically for rows and columns
          Flexible(
            flex: 4,
            child: Container(
              color: Colors.red[850],
              child: Align(
                alignment: Alignment.center,
                //Imported function from rect_image.dart
                //Child calls the function for shape of image
                child: RectangleImage(),
              ),
            ),
          ),
          Flexible(
            flex: 6,
            child: Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: Column(
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            decoration: new BoxDecoration(
                                color: Color.fromRGBO(218, 107, 107, 1),
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(80.0))
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>
                                [
                                  Icon(Icons.camera_roll, color: Colors.yellow, // PHOTO section
                                    size: 50,),
                                  Text('Capture', style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 22
                                  ))
                                ],
                              ),
                            ),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            color: Color.fromRGBO(211, 96, 96, 1),
                            child: Align(
                              alignment: Alignment.center,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>
                                [
                                  Icon(Icons.calendar_today, color: Colors.yellow, // Upcoming Events section
                                    size: 50,),
                                  Text('Events', style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 22
                                  ))
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),

                  Flexible(
                    child: Column(
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            decoration: new BoxDecoration(
                                color: Color.fromRGBO(211, 96, 96, 1),
                                borderRadius: new BorderRadius.only(
                                    topRight: const Radius.circular(80.0))
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.settings, color: Colors.yellow, // Settings section
                                    size: 50,),
                                  Text('Settings', style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20
                                  ))
                                ],
                              ),
                            ),
                          ),
                        ),

                        Flexible(
                          child: Container(
                            color: Color.fromRGBO(211, 96, 96, 1),
                            child: Align(
                              alignment: Alignment.center,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.help_outline, color: Colors.yellow, // HELP section
                                    size: 50,),
                                  Text('Help', style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20
                                  ))
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget dashboard(context)
  {
    return AnimatedPositioned(

      top: 0, // is menu closed for top and bottom
      bottom: 0,
      left: isMenuClosed ? 0 : screenWidth - screenWidth * 0.15,
      right: isMenuClosed ? 0 : -90,
      duration: Duration(milliseconds: 300),
      child: Material(
        elevation: 8,
        color: Colors.blueAccent[850],
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    child: Icon(Icons.menu, color: Colors.grey, size: 40,),
                    onTap: () {
                      setState(() {
                        isMenuClosed = !isMenuClosed;
                      });
                    },
                  ),
                  Text("Greetings! from Group 2",
                      style: TextStyle(fontSize: 20, color: Colors.green)
                  ),
                  Icon(Icons.grade, color: Colors.orange, size: 40,)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}